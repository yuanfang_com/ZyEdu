package com.example.zyedu.activity.login;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.example.zyedu.R;
import com.example.zyedu.activity.main.MainActivity;
import com.example.zyedu.base.BaseActicvity;
import com.example.zyedu.tools.JumpUtil;

/**
 * Created by 半生瓜 on 2017/8/4.
 */


public class LoginActivity extends BaseActicvity<LoginPresenter, LoginModel> implements LoginContract.View {

    private EditText login_id_et;
    private EditText login_ps_et;
    private CheckBox login_rpsw_cb;

    //todo 预留按键
    private TextView login_register_tv;
    private TextView login_foget_tv;
    private Button login_login_bt;

    @Override//todo 绑定xml布局
    public int getLayoutId() {
        return R.layout.login_layout;

    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override//todo 绑定控件地址
    public void initview() {
        login_id_et = (EditText) findViewById(R.id.login_id_et);
        login_ps_et = (EditText) findViewById(R.id.login_ps_et);
        login_rpsw_cb = (CheckBox) findViewById(R.id.login_rpsw_cb);

        login_foget_tv = (TextView) findViewById(R.id.login_foget_tv);
        login_register_tv = (TextView) findViewById(R.id.login_register_tv);
        login_login_bt = (Button) findViewById(R.id.login_login_bt);
        login_login_bt.setOnClickListener(this);

        login_rpsw_cb.setOnCheckedChangeListener(rpsw_cb);
        login_register_tv.setOnClickListener(this);
        Intent intent=getIntent();
        if(intent.getIntExtra("requsecode",0)==1){
            login_id_et.requestFocus();
            login_id_et.setError("注册成功请尝试登入");
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override//todo 重写基类方法，用作按键监听
    public void processClick(View view) {
        switch (view.getId()) {
            case R.id.login_login_bt:
                sentencedtoempty();
                break;
            case R.id.login_register_tv:
                JumpUtil.toRegister(c);
        }
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void sentencedtoempty() {
        //// TODO: 2017/8/7 判断

        if (login_id_et.getText().toString().trim().length() == 0) {
            login_id_et.requestFocus();
            login_id_et.setError("用户名不能为空");
        } else {
            if (login_ps_et.getText().toString().trim().length() == 0) {
                login_ps_et.requestFocus();
                login_ps_et.setError("密码不能为空");
            } else {
                //todo dologin
                presenter.dologin(login_id_et.getText().toString().trim(),
                        login_ps_et.getText().toString().trim());
            }
        }
    }
    @Override//todo 绑定控件后执行的操作

    public void init() {
        presenter.onCreate(this);
    }


    @Override//todo  返回当前上下文contxet
    public Context getContext() {
        return this;
    }

    @Override//// TODO: 2017/8/16 保存jwt
    public void saveJWT(String jwt) {

    }



    @Override//todo 读取缓存结果，初始化et与cb
    public void onCheckBoxResult(boolean remembercode, String username, String password) {
        login_rpsw_cb.setChecked(remembercode);

        login_id_et.setText(username);
        login_ps_et.setText(password);
    }

    @Override//// TODO: 2017/8/7  登入成功后回调方法
    public void loginSuccess(String a) {
        presenter.savaRecord(login_rpsw_cb.isChecked(),  login_id_et.getText().toString().trim(),
        login_ps_et.getText().toString().trim());
        JumpUtil.to(this, MainActivity.class);
        finish();
    }

    @Override//todo 登入失败回调方法
    public void loginFail(String a) {
        if(a.equals("密码错误")){
            login_ps_et.requestFocus();
            login_ps_et.setError("密码错误");

        }else if(a.equals("用户不存在")){
            login_id_et.requestFocus();
            login_id_et.setError("用户不存在");
        }else{
            login_id_et.requestFocus();
            login_id_et.setError("登入失败");
        }
    }

    //todo cb监听器
    private CompoundButton.OnCheckedChangeListener rpsw_cb = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if (b) {
                if (b == true) {

                    login_rpsw_cb.setChecked(true);

                }
            }
        }
    };

}
