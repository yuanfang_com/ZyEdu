package com.example.zyedu.activity.login;

import android.content.Context;

import com.example.zyedu.base.BaseModel;
import com.example.zyedu.base.BasePresenter;
import com.example.zyedu.base.Baseview;
import com.example.zyedu.entity.Login_result;
import com.example.zyedu.entity.User;

import rx.Observable;

/**
 * Created by 半生瓜 on 2017/8/4.
 */

public interface LoginContract {
    interface Model extends BaseModel {
        //                Observable<Register_result> getLoginModel(User user);
        Observable<Login_result> getLoginModel(User user);

        //todo 添加被观察者接口 由model层重写 于P层被rxmanage调用
    }

    interface View extends Baseview {
        //todo 添加本activity 功能接口
        void loginSuccess(String a);

        void loginFail(String a);

        Context getContext();

        void saveJWT(String jwt);

        //todo 初始化et与cb
        public abstract void onCheckBoxResult(boolean remembercode,  String username, String password);
    }

    //todo P层通过泛型绑定M,V层，
    abstract class Presenter extends BasePresenter<Model, View> {
        //todo 登入抽象类
        protected abstract void dologin(String un, String psd);

        //todo 缓存抽象类 重写方法为将et与cb记录存入缓存
        public abstract void savaRecord(Boolean r,  String un, String psd);

        //todo P层初始化接口（1.在Activity初始化时读取sp中内容后做判定）
        public abstract void onCreate(Context context);


    }

}
