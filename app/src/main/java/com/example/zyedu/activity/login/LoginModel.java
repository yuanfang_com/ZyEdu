package com.example.zyedu.activity.login;

import com.example.zyedu.api.HttpMethosds;
import com.example.zyedu.entity.Login_result;
import com.example.zyedu.entity.User;
import com.example.zyedu.tools.RxSchedulers;

import rx.Observable;

/**
 * Created by 半生瓜 on 2017/8/4.
 */

public class LoginModel  implements LoginContract.Model {
    @Override
//        public Observable<Register_result> getLoginModel(User user) {
   //// TODO: 2017/8/21  创建被观察者对象 返回一个被观察者结果
   /// todo （格式：Observable<解析结果类型>方法名（传入类型 传入类型对象 ）{return 网络服务单例对象.所需要的进行的网络请求.开启Rx线程}）
    public Observable<Login_result> getLoginModel(User user) {
        return HttpMethosds.getInstance().retrofitServicel
                .login(user)
                .compose(RxSchedulers.io_main());
    }
}
