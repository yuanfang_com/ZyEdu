package com.example.zyedu.activity.login;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.example.zyedu.entity.Login_result;
import com.example.zyedu.entity.User;

/**
 * Created by 半生瓜 on 2017/8/4.
 */

public class LoginPresenter extends LoginContract.Presenter {
    private String userid;
    private String password;
    private String jwt;
    private String user_name;

   // @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override//todo 绑定观察者，网络请求与m层执行，由p层判断，v层响应
    protected void dologin(String un, String psd) {
        userid = un;
        password = psd;
        rxManager.add(modle.getLoginModel(new User(un, psd))
                .subscribe(result -> {
                            if(result.getStatus().equals("0")){
                            jwt = result.getJwt();
                            user_name = result.getValue();
                            view.loginSuccess(result.getDes());}
                            else{
                                Log.i("1", "555555555555"+result.getDes());
                                view.loginFail(result.getDes());
                            }
                        },
                        e -> {
                            Log.i("test2", e.getMessage());
                        }));
    }

    @Override//todo 保存记录结果，由v层调用该方法
    public void savaRecord(Boolean r, String un, String psd) {
        Log.i("r",String.valueOf(r));

        spTools.setString("jwt", jwt);
        spTools.setString("Name", user_name);
        if (r) {
            spTools.setBoolean("remember", true);
            spTools.setString("username", un);
            spTools.setString("password", psd);



        } else {

            spTools.setBoolean("remember", false);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(Context context) {

        boolean choseRemember = spTools.getBoolean("remember");

        Log.i("remember读取",String.valueOf(choseRemember));

        //// TODO: 2017/7/19
        if (choseRemember) {
            String username = spTools.getString("username");
            String password = spTools.getString("password");
            view.onCheckBoxResult(choseRemember, username, password);

        }
    }
}

