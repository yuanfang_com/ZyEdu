package com.example.zyedu.activity.main;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.zyedu.R;
import com.example.zyedu.adapter.MainPageview;
import com.example.zyedu.base.BaseActicvity;
import com.example.zyedu.fragment.Report.ReportFragment;
import com.example.zyedu.fragment.daily.DailyFragment;
import com.example.zyedu.fragment.history.HistoryFragment;
import com.example.zyedu.tools.JumpUtil;

import java.util.ArrayList;

/**
 * Created by 半生瓜 on 2017/7/20.
 */

public class MainActivity extends BaseActicvity<MainPresenter, MainModel> implements MainContract.view {
    TabLayout main_tablayout;
    Toolbar main_toolbar;
    ViewPager main_viewPager;
    TextView username;
    private View hearlayout;
    private NavigationView navigationView;
    //todo 三个tab对象
    MainPageview mainPageview;
    DailyFragment dailyFragment;
    ReportFragment reportActivity;
    HistoryFragment historyFragment;
    ArrayList<Fragment> fragments;
    ArrayList<Integer> img;
    ArrayList<Integer> img1;
    ArrayList<String> tab_text;
    DrawerLayout drawer;
    @Override
    public int getLayoutId() {
        return R.layout.activity_sidebar;
    }

    @Override
    public void initview() {
        if(spTools.getString("Name").equals("")&&spTools.getString("jwt").equals("")){
            JumpUtil.toLogin(c);
            finish();
        }
        navigationView= (NavigationView) findViewById(R.id.nav_view);
        hearlayout=navigationView.inflateHeaderView(R.layout.nav_header_sidebar);
        username= (TextView) hearlayout.findViewById(R.id.main_sl_name);
        main_tablayout= (TabLayout) findViewById(R.id.main_tablayout);
        main_toolbar= (Toolbar) findViewById(R.id.main_toolbar);
        main_toolbar.setTitle("");
        setSupportActionBar(main_toolbar);
        main_viewPager= (ViewPager) findViewById(R.id.main_viewpager);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        //todo drawerlayout布局侧滑栏按键
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, main_toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_schedule:
                        main_viewPager.setCurrentItem(0);
                        drawer.closeDrawer(navigationView);
                        break;
                    case R.id.nav_report:
                        main_viewPager.setCurrentItem(1);
                        drawer.closeDrawer(navigationView);
                        break;
                    case R.id.nav_history:
                        main_viewPager.setCurrentItem(2);
                        drawer.closeDrawer(navigationView);
                        break;
                    case R.id.nav_manage:
                        break;
                    case R.id.nav_share:
                        JumpUtil.toLogin(c);
                        finish();
                        break;
                    case R.id.nav_send:
                        System.exit(0);
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public void init() {
        username.setText(spTools.getString("Name"));
        // TODO: 2017/8/4  使用适配器绑定TbaLayout和Viewpage的数据，并关联
        dailyFragment= new DailyFragment();
        reportActivity=new ReportFragment();
        historyFragment=new HistoryFragment();
        //// TODO: 2017/8/11 frag集合
        fragments=new ArrayList<Fragment>();
        fragments.add(dailyFragment);
        fragments.add(reportActivity);
        fragments.add(historyFragment);
        //// TODO: 2017/8/11 img集合
        img=new ArrayList<Integer>();
        img.add(R.mipmap.schedule);
        img.add(R.mipmap.report);
        img.add(R.mipmap.history);
        img1=new ArrayList<Integer>();
        img1.add(R.mipmap.unschedule);
        img1.add(R.mipmap.unreport);
        img1.add(R.mipmap.unhistory);
        // TODO: 2017/8/11 标题集合
        tab_text=new ArrayList<String>();
        tab_text.add("进度");
        tab_text.add("报表");
        tab_text.add("历史");
        // TODO: 2017/8/11 vp\tablay\adpter、绑定
        mainPageview=new MainPageview(getSupportFragmentManager(),fragments);
        main_viewPager.setAdapter(mainPageview);
        main_viewPager.setOffscreenPageLimit(2);
        main_tablayout.setupWithViewPager(main_viewPager);
        setupTabIcons();
        // TODO: 2017/8/7 tablayout是否焦点样式
        main_tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                main_tablayout.getTabAt(tab.getPosition()).setIcon(img.get(tab.getPosition()));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                main_tablayout.getTabAt(tab.getPosition()).setIcon(img1.get(tab.getPosition()));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    // TODO: 2017/8/7 初始化tab标签
    private void setupTabIcons() {
        main_tablayout.getTabAt(0).setIcon(img.get(0));
        main_tablayout.getTabAt(1).setIcon(img1.get(1));
        main_tablayout.getTabAt(2).setIcon(img1.get(2));
    }

    @Override
    public void processClick(View view) {

    }

    // todo 右上角选项按键设置
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_sidebar_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_schedule:
                main_viewPager.setCurrentItem(0);
                break;
            case R.id.nav_report:
                main_viewPager.setCurrentItem(1);
                break;
            case R.id.nav_history:
                main_viewPager.setCurrentItem(2);
                break;
            case R.id.nav_manage:
                break;
            case R.id.nav_share:
                JumpUtil.toLogin(c);
                finish();
                break;
            case R.id.nav_send:
                System.exit(0);
                break;
        }
        return true;
    }

}