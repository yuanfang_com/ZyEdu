package com.example.zyedu.activity.main;

import com.example.zyedu.base.BaseModel;
import com.example.zyedu.base.BasePresenter;
import com.example.zyedu.base.Baseview;

/**
 * Created by 半生瓜 on 2017/7/20.
 */

public interface MainContract {
    interface Model extends BaseModel {

    }

    interface view extends Baseview {

    }

    public abstract class Presenter extends BasePresenter<Model, view> {

    }
}
