package com.example.zyedu.activity.register;

import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.zyedu.R;
import com.example.zyedu.activity.login.LoginActivity;
import com.example.zyedu.base.BaseActicvity;
import com.example.zyedu.entity.Register_result;
import com.example.zyedu.tools.JumpUtil;

/**
 * Created by 半生瓜 on 2017/8/7.
 */

public class RegisterActvity extends BaseActicvity<RegisterPresenter, RegisterModel> implements RegisterConteact.View {

    private Toolbar toolbar;

    private EditText reg_name_et;

    private EditText reg_phone_et;

    private EditText reg_psw;

    private EditText reg_psw2;

    private Button reg_sure_bt;

    @Override
    public int getLayoutId() {
        return R.layout.register_layout;
    }

    @Override
    public void initview() {
        reg_name_et = (EditText) findViewById(R.id.reg_name_et);
        reg_phone_et = (EditText) findViewById(R.id.reg_phone_et);
        reg_psw = (EditText) findViewById(R.id.reg_psw);
        reg_psw2 = (EditText) findViewById(R.id.reg_psw2);
        toolbar = (Toolbar) findViewById(R.id.register_toolbar);
        reg_sure_bt = (Button) findViewById(R.id.reg_sure_bt);
        reg_sure_bt.setOnClickListener(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //在toolbar上设置返回按钮
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void processClick(View view) {

        switch (view.getId()) {
            case R.id.reg_sure_bt:
                sentenced_to_empty();
        }
    }


    @Override
    public void init() {
    }

    private void sentenced_to_empty() {
        if (reg_name_et.getText().length() == 0) {
            reg_name_et.requestFocus();
            reg_name_et.setError("请输入姓名");
            return;
        }
        if (reg_phone_et.getText().length() == 0) {
            reg_phone_et.requestFocus();
            reg_phone_et.setError("请输入手机号");
            return;
        }
        if (reg_phone_et.getText().length()!= 11) {
            reg_phone_et.requestFocus();
            reg_phone_et.setError("请输入正确格式的手机号");
            return;
        }
        if (reg_psw.getText().length() == 0) {
            reg_psw.requestFocus();
            reg_psw.setError("请输入密码");
            return;
        }

        if (reg_psw2.getText().length() == 0) {
            reg_psw2.requestFocus();
            reg_psw2.setError("请再次输入密码");
            return;
        }
        if (!reg_psw.getText().toString().trim().equals(reg_psw2.getText().toString().trim())) {
            reg_psw2.requestFocus();
            reg_psw2.setError("两次输入密码不一致");
            return;
        }
        //todo 注册
        presenter.doreg(reg_phone_et.getText().toString().trim(),reg_name_et.getText().toString().trim(),reg_psw.getText().toString().trim());
    }


    @Override
    public void success() {
       JumpUtil.toLogin1(c);
        finish();
    }

    @Override
    public void result(String a) {
         if(a.equals("用户已存在")){
             reg_phone_et.requestFocus();
             reg_phone_et.setError("用户已存在");
             //todo 登入
         }else {
             reg_name_et.requestFocus();
             reg_name_et.setError("链接失败");
         }
    }
}


