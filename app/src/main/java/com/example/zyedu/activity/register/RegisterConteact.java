package com.example.zyedu.activity.register;

import com.example.zyedu.base.BaseModel;
import com.example.zyedu.base.BasePresenter;
import com.example.zyedu.base.Baseview;
import com.example.zyedu.entity.Register_result;
import com.example.zyedu.entity.Reg_user;

import rx.Observable;

/**
 * Created by 半生瓜 on 2017/8/7
 * .
 */

public interface RegisterConteact {
    interface Model extends BaseModel{
        Observable<Register_result> getregisterModel(Reg_user reg_user);
    }
    interface View extends Baseview{
        void success();
        void result(String a);
    }
    abstract  class Presenter extends BasePresenter<RegisterConteact.Model,RegisterConteact.View>{
      protected  abstract void  doreg(String account,String name,String password);
    }
}
