package com.example.zyedu.activity.register;

import com.example.zyedu.api.HttpMethosds;
import com.example.zyedu.entity.Register_result;
import com.example.zyedu.entity.Reg_user;
import com.example.zyedu.tools.RxSchedulers;

import rx.Observable;

/**
 * Created by 半生瓜 on 2017/8/7.
 */

public class RegisterModel implements RegisterConteact.Model  {
    @Override
    public Observable<Register_result> getregisterModel(Reg_user reg_user) {
        return HttpMethosds.getInstance().retrofitServicel
                .regsiter(reg_user)
                .compose(RxSchedulers.io_main());
    }
}
