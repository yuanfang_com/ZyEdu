package com.example.zyedu.adapter;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class MainPageview extends FragmentPagerAdapter {
    // TODO: 2017/8/4  适配器类绑定Viewpage和TabLayout数据
    private Context c;
    private ArrayList<Fragment> fragments;
    ArrayList<String>titles;

    public MainPageview(FragmentManager fragmentManager,ArrayList<Fragment> fragments) {
        super(fragmentManager);
        this.fragments = fragments;

    }
    public void setTitles(ArrayList<String>titles){
        this.titles=titles;
    }
    @Override
    public Fragment getItem(int position) {
        return fragments == null ? null : fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments == null ? 0 : fragments.size();
    }
    //
    @Override
    public CharSequence getPageTitle(int position) {
        return titles==null?null:titles.get(position);
    }
}


