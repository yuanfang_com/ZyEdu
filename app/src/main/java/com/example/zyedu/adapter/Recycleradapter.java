package com.example.zyedu.adapter;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.zyedu.R;
import com.example.zyedu.entity.Bean;
import com.example.zyedu.entity.Requstreport;
import com.example.zyedu.entity.TeacherReportDayVO;
import com.example.zyedu.fragment.history.his_rep.History_rep_Presenter;
import com.example.zyedu.tools.ExpandableViewHoldersHisrep;

import java.text.NumberFormat;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by acer on 2017/8/16.
 */

public class Recycleradapter extends RecyclerView.Adapter<Recycleradapter.HisrepViewHolder> {
    Context context;
    List<TeacherReportDayVO> list;
    RecyclerView recyclerView;
    History_rep_Presenter presenter;
    public static int state;
    private ExpandableViewHoldersHisrep.OpenItem keepOne = new ExpandableViewHoldersHisrep.OpenItem<HisrepViewHolder>();

    public Recycleradapter(Context context, List<TeacherReportDayVO> list, RecyclerView recyclerView, History_rep_Presenter presenter, int state) {
        this.context = context;
        this.list = list;
        this.recyclerView = recyclerView;
        this.presenter = presenter;
        this.state = state;
    }

    @Override
    public HisrepViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        HisrepViewHolder holder = new HisrepViewHolder(LayoutInflater.from(context).inflate(R.layout.his_rep_item, parent, false));
        return holder;
    }

    @Override
    public void onBindViewHolder(HisrepViewHolder holder, int position) {
        holder.bind(position);
        if (state == 1) {
            holder.his_rep_name.setText(list.get(position).getTeacherName());
            holder.his_rep_am.setText(list.get(position).getSum_am());
            holder.his_rep_pm.setText(list.get(position).getSum_pm());
            holder.his_rep_ws.setText(list.get(position).getSum_ws());
            holder.his_rep_date.setText(list.get(position).getDate_years() + "-" + list.get(position).getDate_mouth());
        } else {
            holder.his_rep_name.setText(list.get(position).getTeacherName());
            holder.his_rep_am.setText(list.get(position).getDate_Am());
            holder.his_rep_pm.setText(list.get(position).getDate_Pm());
            holder.his_rep_ws.setText(list.get(position).getDate_Ws());
            holder.his_rep_date.setText(list.get(position).getDate_years() + "-" + list.get(position).getDate_mouth() + "-" + list.get(position).getDate_day());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public class HisrepViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, ExpandableViewHoldersHisrep.Expandable, ExpandableViewHoldersHisrep.ExpandableTop {
        TextView his_rep_name;
        TextView his_rep_am;
        TextView his_rep_pm;
        TextView his_rep_ws;
        TextView his_rep_date;
        Button his_rep_sum;
        Button his_rep_deta;
        LinearLayout his_rep_boom;
        LinearLayout his_rep_top;
        View his_rep_view;

        public HisrepViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
            his_rep_name = (TextView) view.findViewById(R.id.his_rep_name);
            his_rep_am = (TextView) view.findViewById(R.id.his_rep_am);
            his_rep_pm = (TextView) view.findViewById(R.id.his_rep_pm);
            his_rep_ws = (TextView) view.findViewById(R.id.his_rep_ws);
            his_rep_date = (TextView) view.findViewById(R.id.his_rep_date);
            his_rep_sum = (Button) view.findViewById(R.id.his_rep_sum);
            his_rep_deta = (Button) view.findViewById(R.id.his_rep_deta);
            his_rep_boom = (LinearLayout) view.findViewById(R.id.his_rep_boom);
            his_rep_top = (LinearLayout) view.findViewById(R.id.his_rep_top);
            his_rep_view = view.findViewById(R.id.his_rep_view);
            his_rep_sum.setOnClickListener(this);
            his_rep_deta.setOnClickListener(this);
        }

        public View getSubmitView() {
            return his_rep_boom;
        }

        public View getTopView() {
            return his_rep_view;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.his_rep_sum:
                    String date = list.get(getAdapterPosition()).getReportDate();
                    String sum = list.get(getAdapterPosition()).getSum();
                    String pacenpage = String.valueOf(NumberFormat.getPercentInstance().format((float) Integer.parseInt(sum) / Integer.parseInt(date)));
                    String[] items = new String[]{"当月应报课时:" + date, "当月实报课时:" + sum, "当月报课率为:" + pacenpage};
                    new AlertDialog.Builder(context).setTitle("课时信息").setItems(items, null).setNeutralButton("确定", null).show();
                    break;
                case R.id.his_rep_deta:
                    String year_mouth = his_rep_date.getText().toString();
                    presenter.requstrepday(new Bean<Requstreport>(context)
                            .addDate(new Requstreport(his_rep_name
                                    .getText().toString()
                                    , year_mouth.substring(0, 4)
                                    , year_mouth.substring(5, 7),
                                    "")));
                    keepOne.toggle(this);
                    break;
                default:
                    if (state == 1)
                        keepOne.toggle(this);
            }
        }

        public void bind(int pos) {
            keepOne.bind(this, pos);

        }

        @Override
        public View getExpandView() {
            return his_rep_boom;
        }

        @Override
        public View getExpandTopView() {
            return his_rep_view;
        }
    }
}
