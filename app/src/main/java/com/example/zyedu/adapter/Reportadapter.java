package com.example.zyedu.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.zyedu.R;
import com.example.zyedu.view.ClassDateView;

import java.util.ArrayList;

/**
 * Created by acer on 2017/8/8.
 */

public class Reportadapter extends BaseAdapter {
    Context context;
    ArrayList<String> list;
    ViewHolder vh;
    ClassDateView classDateView;
    public Reportadapter(Context context, ArrayList<String> list,ClassDateView classDateView){
        this.context=context;
        this.list=list;
        this.classDateView=classDateView;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            vh = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.list_view, null);
            vh.textView= (TextView) convertView.findViewById(R.id.report_date);
            vh.checkBox= (CheckBox) convertView.findViewById(R.id.report_moning);
            vh.checkBox1= (CheckBox) convertView.findViewById(R.id.report_afternoon);
            vh.checkBox2= (CheckBox) convertView.findViewById(R.id.report_night);
            vh.button= (Button) convertView.findViewById(R.id.report_del);
            vh.checkBox.isChecked();
            // TODO: 2017/8/11 单击删除列表项指定列表项
            vh.button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int delitem=Integer.parseInt(list.get(position).substring(8,10));
                    list.remove(position);
                    notifyDataSetChanged();
                    classDateView.selectday[delitem]=0;
                    classDateView.invalidate();
                }
            });
            convertView.setTag(vh);
        }else {
            vh = (ViewHolder) convertView.getTag();
        }
        vh.textView.setText(list.get(position));
        return convertView;
    }
    class ViewHolder
    {
        TextView textView;
        CheckBox checkBox;
        CheckBox checkBox1;
        CheckBox checkBox2;
        Button button;
    }

    // TODO: 2017/8/11 动态加载列表项
    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}
