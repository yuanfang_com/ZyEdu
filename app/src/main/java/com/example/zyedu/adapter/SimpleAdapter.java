package com.example.zyedu.adapter;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.zyedu.R;
import com.example.zyedu.entity.His_dai_result_a;
import com.example.zyedu.tools.ExpandableViewHoldersUtil;

import java.util.List;

import butterknife.ButterKnife;


/**
 * Created by JunX on 17/5/13.
 */

public class SimpleAdapter extends RecyclerView.Adapter<SimpleAdapter.SimpleViewHolder> {

    private List<His_dai_result_a> data;
    private Context context;
    private ExpandableViewHoldersUtil.OpenItem keepOne = new ExpandableViewHoldersUtil.OpenItem<SimpleViewHolder>();


    public SimpleAdapter(Context context, List<His_dai_result_a> data) {

        this.context = context;
        this.data = data;

    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_simple, parent, false);
        SimpleViewHolder holder = new SimpleViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        //todo 数据绑定

        holder.item_time.setText(data.get(position).getReportDate());
        holder.item_data.setText(data.get(position).getCreateTime().split(" ")[1]);
        holder.item_name.setText(data.get(position).getTeacherName());
        holder.item_class.setText(data.get(position).getCourseName());
        holder.item_stage.setText(data.get(position).getCourseStage());
        holder.item_student_num.setText(data.get(position).getNow_num() + "/" + data.get(position).getOpen_num());

        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, ExpandableViewHoldersUtil.Expandable, ExpandableViewHoldersUtil.ExpandableTop {

        Button b;
        TextView item_time;

        TextView item_data;

        TextView item_name;

        TextView item_class;

        TextView item_stage;

        TextView item_student_num;


        LinearLayout submit_view;

        RelativeLayout top_view;

        View line;

        public SimpleViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
            item_time = (TextView) view.findViewById(R.id.item_time);
            item_data = (TextView) view.findViewById(R.id.item_data);
            item_name = (TextView) view.findViewById(R.id.item_name);
            item_class = (TextView) view.findViewById(R.id.item_class);
            item_stage = (TextView) view.findViewById(R.id.item_stage);
            item_student_num = (TextView) view.findViewById(R.id.item_student_num);
            submit_view = (LinearLayout) view.findViewById(R.id.submit_view);
            top_view = (RelativeLayout) view.findViewById(R.id.top_view);
            line = (View) view.findViewById(R.id.line);
            b = (Button) view.findViewById(R.id.item_bt);
            b.setOnClickListener(this);


        }

        public View getSubmitView() {
            return submit_view;
        }

        public View getTopView() {
            return top_view;
        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.item_bt:
                    new AlertDialog.Builder(context).setTitle("日报详情").setItems(
                            new String[]{
                                    "教师姓名：" + data.get(getAdapterPosition()).getTeacherName(),
                                    "课程名称："+ data.get(getAdapterPosition()).getCourseName(),
                                    "课程阶段："+ data.get(getAdapterPosition()).getCourseStage(),
                                    "开课人数："+ data.get(getAdapterPosition()).getOpen_num(),
                                    "现人数  ："+ data.get(getAdapterPosition()).getNow_num(),
                                    "增加人数："+ data.get(getAdapterPosition()).getAddnum(),
                                    "工作内容："+ data.get(getAdapterPosition()).getReportContent(),
                            }, null
                    ).setNeutralButton("确定", null).show();
                    break;
                default:
                    keepOne.toggle(this);
            }
        }

        public void bind(int pos) {
            keepOne.bind(this, pos);

        }

        @Override
        public View getExpandView() {
            return submit_view;
        }

        @Override
        public View getExpandTopView() {
            return top_view;
        }
    }

}
