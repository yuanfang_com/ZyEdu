package com.example.zyedu.api;

import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by 半生瓜 on 2017/7/18.
 * http://gank.io/post/56e80c2c677659311bed9841 (参考资料)
 * //todo 网络请求工具
 */

public class HttpMethosds {
    // TODO: 2017/8/21 基URL
    public static final String BASE_URL = "http://192.168.191.1:8080/app/";
    //    public static final String BASE_URL = Properties.testURL;
    // TODO: 2017/8/21 网络请求超时时间
    private static final int DEFAULT_TIMEOUT = 5;

    private Retrofit retrofit;

    public RetrofitService retrofitServicel;
   //todo 初始化拦截器 添加请求头
    Interceptor mInterceptor = (chain) -> chain.proceed(chain.request().newBuilder()
            .addHeader("Content-Type", "application/json")
            .build());

    private HttpMethosds() {


        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder()
                //todo 添加拦截器
                .addInterceptor(mInterceptor)
                // TODO: 2017/8/21 设置超时
                .connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS);


        retrofit = new Retrofit.Builder()
                // TODO: 2017/8/21 设置网络请求对象
                .client(httpClientBuilder.build())
                // TODO: 2017/8/21 设置json打包解析工具
                .addConverterFactory(GsonConverterFactory.create())
                //// TODO: 2017/8/21  设置适配器
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                // TODO: 2017/8/21 添加基网络请求地址
                .baseUrl(BASE_URL)
                .build();
        retrofitServicel = retrofit.create(RetrofitService.class);

    }

    // TODO: 2017/8/21 创建单例模式
    private static class SingletonHolder {
        private static final HttpMethosds INSTANCE = new HttpMethosds();
    }
    public static HttpMethosds getInstance() {
        return SingletonHolder.INSTANCE;
    }
}
