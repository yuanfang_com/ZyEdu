package com.example.zyedu.api;

import com.example.zyedu.base.BaseResult;
import com.example.zyedu.entity.Bean;
import com.example.zyedu.entity.DailyBean;
import com.example.zyedu.entity.Daily_result;
import com.example.zyedu.entity.His_dai_bean;
import com.example.zyedu.entity.His_dai_result;
import com.example.zyedu.entity.Login_result;
import com.example.zyedu.entity.Reg_user;
import com.example.zyedu.entity.Register_result;
import com.example.zyedu.entity.ReportBean;
import com.example.zyedu.entity.Requstreport;
import com.example.zyedu.entity.TeacherReportDayVO;
import com.example.zyedu.entity.User;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by 半生瓜 on 2017/7/14.
 */
//todo api地址集合
public interface RetrofitService {
    //    @POST("login")
//    Observable<BaseResult<User>> login(@Body User user);
    //// TODO: 2017/8/21
    //todo 格式：@请求类型（尾URL） Observable<解析实体类型> 方法名 (@添加部位 传入类型 传入类型对象名);
    @POST("login")
    Observable<Login_result> login(@Body User user);

    @POST("reg")
    Observable<Register_result> regsiter(@Body Reg_user reg_user);

    @POST("reportclass")
    Observable<BaseResult> reportclass(@Body Bean<ReportBean> beanBean);

    @POST("reportDaily_save")
    Observable<Daily_result> reportDaily(@Body Bean<DailyBean> beanBean);

    @POST("reportcoursemouth_sum")
    Observable<BaseResult<List<TeacherReportDayVO>>> requstrep(@Body Bean<Requstreport> beanBean);
    @POST("reportcoursemouth")
    Observable<BaseResult<List<TeacherReportDayVO>>> requstrepday(@Body Bean<Requstreport> beanBean);

    @POST("reportDaily")
    Observable<His_dai_result> hisDaily(@Body Bean<His_dai_bean> beanBean);
}
