package com.example.zyedu.base;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;

import com.example.zyedu.tools.SpTools;
import com.example.zyedu.tools.TUtil;
import com.example.zyedu.tools.Ts;

/**
 * Created by 半生瓜 on 2017/7/12.
 */

public abstract class BaseActicvity<P extends BasePresenter, M extends BaseModel> extends AppCompatActivity implements View.OnClickListener {
    public Context c;
    //// TODO: 2017/8/21  上下文
    public P presenter;
    //// TODO: 2017/8/21 p层m层对象
    public M model;
    // TODO: 2017/8/21  toast对象初始化
    public Ts t = new Ts(this);
    // TODO: 2017/8/21  sp缓存对象
    public SpTools spTools;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(this.getLayoutId());
        //todo 创建上下文对象c 在子类之中可以准确调用baseA上下文
        c = this;
        //// TODO: 2017/8/21 返回泛型参数中实际实现类的对象
        presenter = TUtil.getT(this, 0);
        model = TUtil.getT(this, 1);
        //todo 将对象地址传入 p层中
        if (this instanceof Baseview) presenter.setVM(c, this, model);
        //todo 初始化sp缓存工具
        spTools = new SpTools(c);
        //todo 初始化控件
        this.initview();
        //todo 初始化视图（完成沉浸式布局）
        this.initWindow();
        //todo 在onCreate 最后执行的部分
        this.init();
    }
    //todo 初始化视图（完成沉浸式布局）
    void initWindow() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            View decorView = getWindow().getDecorView();
            int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
            decorView.setSystemUiVisibility(option);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(Color.TRANSPARENT);
            }
        }


    }
    //todo 实现onclick接口方法
    @Override
    public void onClick(View view) {
        processClick(view);
    }

    //todo 该抽象类由子类实现 用于实现onclick接口方法
    public abstract void processClick(View view) ;

    public abstract int getLayoutId();
    //todo 初始化控件
    public abstract void initview();
    //todo 在onCreate 最后执行的部分
    public abstract void init();


}
