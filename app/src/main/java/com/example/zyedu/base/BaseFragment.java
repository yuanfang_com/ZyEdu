package com.example.zyedu.base;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.zyedu.tools.SpTools;
import com.example.zyedu.tools.TUtil;
import com.example.zyedu.tools.Ts;

/**
 * Created by 半生瓜 on 2017/7/21.
 * 详情参考BaseActivity
 */

public abstract class BaseFragment<P extends BasePresenter, M extends BaseModel> extends Fragment implements View.OnClickListener {

    public P presenter;
    public M model;
    public Context context;
    public Ts ts;
    public SpTools spTools;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        context = getContext();
        presenter = TUtil.getT(this, 0);
        model = TUtil.getT(this, 1);
        if (this instanceof Baseview) presenter.setVM(context, this, model);
        ts = new Ts(getContext());
        spTools = new SpTools(context);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        init();
    }

    protected abstract void init();

    public abstract int getLayoutId();

    public abstract void initView();
}
