package com.example.zyedu.base;

import android.content.Context;
import android.util.Log;

import com.example.zyedu.R;
import com.example.zyedu.tools.L;
import com.example.zyedu.tools.SpTools;
import com.example.zyedu.view.LoadingDialog;

/**
 * Created by 半生瓜 on 2017/7/17.
 */

public abstract class BasePresenter<M, V> {
    // TODO: 2017/8/21 上下文对象
    public Context context;
    // TODO: 2017/8/21 泛型 m层对象
    public M modle;
    // TODO: 2017/8/21 泛型 v层对象
    public V view;

    public SpTools spTools;
    // TODO: 2017/8/21  dialog提示窗口对象
    protected LoadingDialog dialog;
    // TODO: 2017/8/21 RXjava 框架中 自定义Rxjava管理器Rxmanager
    public RxManager rxManager = new RxManager();

    // TODO: 2017/8/21  释放 rx管理器
    public void onDestroy() {
        rxManager.clear();
    }

    // TODO: 2017/8/21 将BaseActivity or BaseFragment 中的对象地址传入 （由其实现的泛型声明类调用）
    public void setVM(Context context, V v, M m) {
        Log.d("test", "setVM");
        this.context = context;
        this.view = v;
        this.modle = m;
        spTools = new SpTools(context);
    }

    // TODO: 2017/8/21  显示dialog
    protected void showLoadingDialog(Boolean isCanClose) {

        if (dialog == null) {
            dialog = new LoadingDialog(context, R.style.dialogBase);
            dialog.setCancelable(isCanClose);
            dialog.setTipText("加载中");
        }
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    // TODO: 2017/8/21 隐藏 dialog
    protected void hideLoadingDialog() {
        /*if(progressDialog != null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }*/
        if (dialog != null && dialog.isShowing()) {
            L.d("关闭加载框");
            dialog.dismiss();
        }

    }


}
