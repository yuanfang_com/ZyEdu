package com.example.zyedu.base;

/**
 * Created by 半生瓜 on 2017/7/19.
 */

public class BaseResult<T> {
    private String status;

    public String getStatus() {
        return status;
    }

    private T data;
    public T getData(){
        return  data;
    }
}
