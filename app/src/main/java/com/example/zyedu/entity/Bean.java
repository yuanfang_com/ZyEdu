package com.example.zyedu.entity;

import android.content.Context;

import com.example.zyedu.tools.SpTools;

/**
 * Created by 半生瓜 on 2017/8/16.
 * * @D解析实体类
 public String jwt;
 public D d;

 public Bean (D d){
 this.d=d;
 }
 */

public class Bean <D>{
    public String jwt;
    public D d;
    public Bean(Context c){
        SpTools spTools=new SpTools(c);
        jwt=spTools.getString("jwt");
    }
    public Bean addDate(D d){
        this.d=d;
        return this;
    }
}
