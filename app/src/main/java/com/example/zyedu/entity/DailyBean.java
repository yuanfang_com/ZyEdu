package com.example.zyedu.entity;

/**
 * Created by 半生瓜 on 2017/8/11.
 */

public class DailyBean {
    String course_stage;
    String report_content;
    String student_num;
    String remark;
    String report_data;
    String course_id;
    String student_situation;

    public DailyBean(String report_data,
                     String course_id,
                     String course_stage,
                     String student_num,
                     String report_content,
                     String student_situation,
                     String remark

    ) {
        this.course_id = course_id;
        this.course_stage = course_stage;
        this.report_content = report_content;
        this.student_num = student_num;
        this.report_data = report_data;
        this.student_situation = student_situation;
        this.remark = remark;

    }

    public String getCourse_stage() {
        return course_stage;
    }

    public void setCourse_stage(String course_stage) {
        this.course_stage = course_stage;
    }

    public String getReport_content() {
        return report_content;
    }

    public void setReport_content(String report_content) {
        this.report_content = report_content;
    }

    public String getStudent_situation() {
        return student_num;
    }

    public void setStudent_situation(String student_situation) {
        this.student_num = student_situation;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getReport_data() {
        return report_data;
    }

    public void setReport_data(String report_data) {
        this.report_data = report_data;
    }


}
