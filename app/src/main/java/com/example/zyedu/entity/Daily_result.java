package com.example.zyedu.entity;

import android.util.Log;

import com.example.zyedu.tools.Jwt_analysis_util;

/**
 * Created by 半生瓜 on 2017/8/21.
 */

public class Daily_result {
    private Object data;
    private String des;
    private String jwt;
    private String status;
    private long time;

    // @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public String getValue()  {
        Log.i("222222222222222222",jwt);
        return Jwt_analysis_util.analysis_name(jwt);
    }
    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
