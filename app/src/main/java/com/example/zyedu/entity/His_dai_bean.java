package com.example.zyedu.entity;

/**
 * Created by 半生瓜 on 2017/8/18.
 */

public class His_dai_bean {

    /**
     * name :
     * year :
     * mouth :
     * day :
     */

    private String name;
    private String year;
    private String mouth;
    private String day;

    public His_dai_bean(String name ,String year,String mouth,String day){
        this.name=name;
        this.year=year;
        this.mouth=mouth;
        this.day=day;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMouth() {
        return mouth;
    }

    public void setMouth(String mouth) {
        this.mouth = mouth;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}
