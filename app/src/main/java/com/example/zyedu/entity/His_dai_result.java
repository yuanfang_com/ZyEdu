package com.example.zyedu.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 半生瓜 on 2017/8/18.
 */

public class His_dai_result {
    private List<His_dai_result_a> data=new ArrayList<>();
    private String des;
    private String jwt;
    private String status;

    public List<His_dai_result_a> getData() {
        return data;
    }

    public String getDes() {
        return des;
    }

    public String getJwt() {
        return jwt;
    }

    public String getStatus() {
        return status;
    }

    public long getTime() {
        return time;
    }

    private long time;
}

