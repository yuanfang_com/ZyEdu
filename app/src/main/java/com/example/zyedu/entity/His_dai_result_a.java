package com.example.zyedu.entity;

/**
 * Created by 半生瓜 on 2017/8/18.
 */

public class His_dai_result_a{

    /**
     * addnum : 1
     * addnumbf : 14.29
     * beginDate :
     * course :
     * courseId : 7
     * courseName : Java第1期
     * courseStage : 第一阶段
     * courseTypeCode :
     * createTime : 2017-07-31 19:08:44
     * currentStudentNum : 0
     * date_Am :
     * date_Pm :
     * date_Ws :
     * date_day :
     * date_mouth :
     * date_years :
     * endDate :
     * id : 24
     * jwt :
     * name :
     * now_num : 8
     * open_num : 7
     * planEndDate :
     * remark : 良好
     * reportContent : 1.吃饭
     2.睡觉
     3.打豆豆
     * reportDate : 2017-07-31
     * reportDateAM : null
     * reportDatePM : null
     * reportDateWS : null
     * reportList : null
     * report_content :
     * status :
     * studentNum : 0
     * studentSituation : 良好
     * sum :
     * sum_am :
     * sum_pm :
     * sum_ws :
     * teacherName : 李纪科
     * teacherUser_id :
     * userId : 2
     */

    private String addnum;
    private String addnumbf;
    private String beginDate;
    private String course;
    private int courseId;
    private String courseName;
    private String courseStage;
    private String courseTypeCode;
    private String createTime;
    private int currentStudentNum;
    private String date_Am;
    private String date_Pm;
    private String date_Ws;
    private String date_day;
    private String date_mouth;
    private String date_years;
    private String endDate;
    private int id;
    private String jwt;
    private String name;
    private String now_num;
    private String open_num;
    private String planEndDate;
    private String remark;
    private String reportContent;
    private String reportDate;
    private Object reportDateAM;
    private Object reportDatePM;
    private Object reportDateWS;
    private Object reportList;
    private String report_content;
    private String status;
    private int studentNum;
    private String studentSituation;
    private String sum;
    private String sum_am;
    private String sum_pm;
    private String sum_ws;
    private String teacherName;
    private String teacherUser_id;
    private int userId;

    public String getAddnum() {
        return addnum;
    }

    public void setAddnum(String addnum) {
        this.addnum = addnum;
    }

    public String getAddnumbf() {
        return addnumbf;
    }

    public void setAddnumbf(String addnumbf) {
        this.addnumbf = addnumbf;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseStage() {
        return courseStage;
    }

    public void setCourseStage(String courseStage) {
        this.courseStage = courseStage;
    }

    public String getCourseTypeCode() {
        return courseTypeCode;
    }

    public void setCourseTypeCode(String courseTypeCode) {
        this.courseTypeCode = courseTypeCode;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public int getCurrentStudentNum() {
        return currentStudentNum;
    }

    public void setCurrentStudentNum(int currentStudentNum) {
        this.currentStudentNum = currentStudentNum;
    }

    public String getDate_Am() {
        return date_Am;
    }

    public void setDate_Am(String date_Am) {
        this.date_Am = date_Am;
    }

    public String getDate_Pm() {
        return date_Pm;
    }

    public void setDate_Pm(String date_Pm) {
        this.date_Pm = date_Pm;
    }

    public String getDate_Ws() {
        return date_Ws;
    }

    public void setDate_Ws(String date_Ws) {
        this.date_Ws = date_Ws;
    }

    public String getDate_day() {
        return date_day;
    }

    public void setDate_day(String date_day) {
        this.date_day = date_day;
    }

    public String getDate_mouth() {
        return date_mouth;
    }

    public void setDate_mouth(String date_mouth) {
        this.date_mouth = date_mouth;
    }

    public String getDate_years() {
        return date_years;
    }

    public void setDate_years(String date_years) {
        this.date_years = date_years;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNow_num() {
        return now_num;
    }

    public void setNow_num(String now_num) {
        this.now_num = now_num;
    }

    public String getOpen_num() {
        return open_num;
    }

    public void setOpen_num(String open_num) {
        this.open_num = open_num;
    }

    public String getPlanEndDate() {
        return planEndDate;
    }

    public void setPlanEndDate(String planEndDate) {
        this.planEndDate = planEndDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getReportContent() {
        return reportContent;
    }

    public void setReportContent(String reportContent) {
        this.reportContent = reportContent;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public Object getReportDateAM() {
        return reportDateAM;
    }

    public void setReportDateAM(Object reportDateAM) {
        this.reportDateAM = reportDateAM;
    }

    public Object getReportDatePM() {
        return reportDatePM;
    }

    public void setReportDatePM(Object reportDatePM) {
        this.reportDatePM = reportDatePM;
    }

    public Object getReportDateWS() {
        return reportDateWS;
    }

    public void setReportDateWS(Object reportDateWS) {
        this.reportDateWS = reportDateWS;
    }

    public Object getReportList() {
        return reportList;
    }

    public void setReportList(Object reportList) {
        this.reportList = reportList;
    }

    public String getReport_content() {
        return report_content;
    }

    public void setReport_content(String report_content) {
        this.report_content = report_content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStudentNum() {
        return studentNum;
    }

    public void setStudentNum(int studentNum) {
        this.studentNum = studentNum;
    }

    public String getStudentSituation() {
        return studentSituation;
    }

    public void setStudentSituation(String studentSituation) {
        this.studentSituation = studentSituation;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }

    public String getSum_am() {
        return sum_am;
    }

    public void setSum_am(String sum_am) {
        this.sum_am = sum_am;
    }

    public String getSum_pm() {
        return sum_pm;
    }

    public void setSum_pm(String sum_pm) {
        this.sum_pm = sum_pm;
    }

    public String getSum_ws() {
        return sum_ws;
    }

    public void setSum_ws(String sum_ws) {
        this.sum_ws = sum_ws;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getTeacherUser_id() {
        return teacherUser_id;
    }

    public void setTeacherUser_id(String teacherUser_id) {
        this.teacherUser_id = teacherUser_id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}

