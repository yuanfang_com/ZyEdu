package com.example.zyedu.entity;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.example.zyedu.tools.Jwt_analysis_util;

/**
 * Created by 半生瓜 on 2017/8/16.
 */

public class Login_result {

    /**
     * data : null
     * des : 用户插入成功
     * jwt : eyJhbGciOiJIUzUxMiJ9.eyJ1c2VybmFtZSI6IjE1NzU5OTkwMTkxIn0.ivaGgUhgSY4hr3nsnX43dr6r_aXFH-Yrd-9Qgw6u-EsXi8ZJCsCdxcCOwD_e1rHwadH9C4LOs4K1ALbQqPabdA
     * status :
     * time : 1502185002513
     */

    private Object data;
    private String des;
    private String jwt;
    private String status;
    private long time;

   // @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public String getValue()  {
        Log.i("222222222222222222",jwt);
        return Jwt_analysis_util.analysis_name(jwt);
    }
    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}

