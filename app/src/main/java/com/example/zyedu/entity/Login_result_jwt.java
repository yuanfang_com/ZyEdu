package com.example.zyedu.entity;

/**
 * Created by 半生瓜 on 2017/8/17.
 */

public class Login_result_jwt {
    /**
     * user_id : 17
     * name : 林皓宇
     * username : 15759990191
     */

    private String user_id;
    private String name;
    private String username;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
