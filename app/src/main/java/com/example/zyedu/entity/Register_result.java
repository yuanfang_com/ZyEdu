package com.example.zyedu.entity;

/**
 * Created by 半生瓜 on 2017/8/8.
 */

public class Register_result {

    /**
     * data : null
     * des : 用户插入成功
     * jwt : eyJhbGciOiJIUzUxMiJ9.eyJ1c2VybmFtZSI6IjE1NzU5OTkwMTkxIn0.ivaGgUhgSY4hr3nsnX43dr6r_aXFH-Yrd-9Qgw6u-EsXi8ZJCsCdxcCOwD_e1rHwadH9C4LOs4K1ALbQqPabdA
     * status :
     * time : 1502185002513
     */

    private Object data;
    private String des;
    private String jwt;
    private String status;
    private long time;

    public String getValue(){
//        return Base64Object.base64ToString(jwt);
        return "";
    }
    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}

