package com.example.zyedu.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 半生瓜 on 2017/8/11.
 */

public class ReportBean {
     List<entity> reportList = new ArrayList<>();
     String course;


    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public void add(String data, String A, String B, String C) {
        reportList.add(new entity(data, A, B, C));
    }

    class entity {
       public String reportDate;
       public String reportDateAM;
       public String reportDatePM;
       public  String reportDateWS;

        public String getReportDate() {
            return reportDate;
        }

        public String getReportDateAM() {
            return reportDateAM;
        }

        public String getReportDatePM() {
            return reportDatePM;
        }

        public String getReportDateWS() {
            return reportDateWS;
        }

        public entity(String data, String A, String B, String C) {
            this.reportDate = data;
            this.reportDateAM = A;
            this.reportDatePM = B;
            this.reportDateWS = C;

        }
    }
}
