package com.example.zyedu.entity;

import lombok.Data;
import org.json.JSONArray;

/**
 * Created by jack  2017/7/29.
 */
public class TeacherReportDayVO {


    private Integer id;
    private Integer userId;
    private String TeacherUser_id;
    private Integer courseId;
    private String courseStage;
    private String reportContent;
    private String studentSituation;
    private String reportDate;
    private String createTime;
    private String remark;
    private String teacherName;
    private String report_content;
    private String name;
    private String course;
    private String courseName;
    private String courseTypeCode;
    private Integer studentNum;
    private Integer currentStudentNum;
    private String beginDate;
    private String planEndDate;
    private String endDate;
    private String status;
    private String date_Am;
    private String date_Pm;
    private String date_Ws;
    private String sum_am;
    private String sum_pm;
    private String sum_ws;
    private String date_years;
    private String date_mouth;
    private String date_day;
    private String jwt;
    private Object reportDateAM;
    private Object reportDatePM;
    private Object reportDateWS;
    private JSONArray reportList;
    private String sum;
    private  String sum_student_num;
    private  String now_student_num;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTeacherUser_id() {
        return TeacherUser_id;
    }

    public void setTeacherUser_id(String teacherUser_id) {
        TeacherUser_id = teacherUser_id;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getCourseStage() {
        return courseStage;
    }

    public void setCourseStage(String courseStage) {
        this.courseStage = courseStage;
    }

    public String getReportContent() {
        return reportContent;
    }

    public void setReportContent(String reportContent) {
        this.reportContent = reportContent;
    }

    public String getStudentSituation() {
        return studentSituation;
    }

    public void setStudentSituation(String studentSituation) {
        this.studentSituation = studentSituation;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getReport_content() {
        return report_content;
    }

    public void setReport_content(String report_content) {
        this.report_content = report_content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseTypeCode() {
        return courseTypeCode;
    }

    public void setCourseTypeCode(String courseTypeCode) {
        this.courseTypeCode = courseTypeCode;
    }

    public Integer getStudentNum() {
        return studentNum;
    }

    public void setStudentNum(Integer studentNum) {
        this.studentNum = studentNum;
    }

    public Integer getCurrentStudentNum() {
        return currentStudentNum;
    }

    public void setCurrentStudentNum(Integer currentStudentNum) {
        this.currentStudentNum = currentStudentNum;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getPlanEndDate() {
        return planEndDate;
    }

    public void setPlanEndDate(String planEndDate) {
        this.planEndDate = planEndDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate_Am() {
        return date_Am;
    }

    public void setDate_Am(String date_Am) {
        this.date_Am = date_Am;
    }

    public String getDate_Pm() {
        return date_Pm;
    }

    public void setDate_Pm(String date_Pm) {
        this.date_Pm = date_Pm;
    }

    public String getDate_Ws() {
        return date_Ws;
    }

    public void setDate_Ws(String date_Ws) {
        this.date_Ws = date_Ws;
    }

    public String getSum_am() {
        return sum_am;
    }

    public void setSum_am(String sum_am) {
        this.sum_am = sum_am;
    }

    public String getSum_pm() {
        return sum_pm;
    }

    public void setSum_pm(String sum_pm) {
        this.sum_pm = sum_pm;
    }

    public String getSum_ws() {
        return sum_ws;
    }

    public void setSum_ws(String sum_ws) {
        this.sum_ws = sum_ws;
    }

    public String getDate_years() {
        return date_years;
    }

    public void setDate_years(String date_years) {
        this.date_years = date_years;
    }

    public String getDate_mouth() {
        return date_mouth;
    }

    public void setDate_mouth(String date_mouth) {
        this.date_mouth = date_mouth;
    }

    public String getDate_day() {
        return date_day;
    }

    public void setDate_day(String date_day) {
        this.date_day = date_day;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public Object getReportDateAM() {
        return reportDateAM;
    }

    public void setReportDateAM(Object reportDateAM) {
        this.reportDateAM = reportDateAM;
    }

    public Object getReportDatePM() {
        return reportDatePM;
    }

    public void setReportDatePM(Object reportDatePM) {
        this.reportDatePM = reportDatePM;
    }

    public Object getReportDateWS() {
        return reportDateWS;
    }

    public void setReportDateWS(Object reportDateWS) {
        this.reportDateWS = reportDateWS;
    }

    public JSONArray getReportList() {
        return reportList;
    }

    public void setReportList(JSONArray reportList) {
        this.reportList = reportList;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }

    public String getSum_student_num() {
        return sum_student_num;
    }

    public void setSum_student_num(String sum_student_num) {
        this.sum_student_num = sum_student_num;
    }

    public String getNow_student_num() {
        return now_student_num;
    }

    public void setNow_student_num(String now_student_num) {
        this.now_student_num = now_student_num;
    }

    public String getAddnumbf() {
        return addnumbf;
    }

    public void setAddnumbf(String addnumbf) {
        this.addnumbf = addnumbf;
    }

    public String getAddnum() {
        return addnum;
    }

    public void setAddnum(String addnum) {
        this.addnum = addnum;
    }

    private  String addnumbf;
    private  String addnum;


}
