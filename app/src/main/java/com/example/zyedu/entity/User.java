package com.example.zyedu.entity;

/**
 * Created by 半生瓜 on 2017/7/12.
 */

public class User {
    private String account;
    private String password;
    public User(String username, String password){
        this.account=username;
        this.password=password;
    }


    public String getUsername() {
        return account;
    }

    public void setUsername(String username) {
        this.account = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }



}
