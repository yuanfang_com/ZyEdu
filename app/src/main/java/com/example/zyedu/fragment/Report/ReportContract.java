package com.example.zyedu.fragment.Report;

import com.example.zyedu.base.BaseModel;
import com.example.zyedu.base.BasePresenter;
import com.example.zyedu.base.BaseResult;
import com.example.zyedu.base.Baseview;
import com.example.zyedu.entity.Bean;
import com.example.zyedu.entity.ReportBean;
import rx.Observable;

/**
 * Created by acer on 2017/8/4.
 */

public interface ReportContract {
    interface Modle extends BaseModel{
        // TODO: 2017/8/11 网络请求提交课时
        Observable<BaseResult> reportclass(Bean<ReportBean> beanBean);
    }
    interface View extends Baseview{
        // TODO: 2017/8/11 网络请求返回结果
        void reportSuccess();

        void reportFail();
    }
    abstract class Presenter extends BasePresenter<Modle,View>{
        // TODO: 2017/8/11 课时提交请求
        protected abstract void reportclass(Bean<ReportBean> beanBean);
    }
}
