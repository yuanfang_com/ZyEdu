package com.example.zyedu.fragment.Report;

import com.example.zyedu.api.HttpMethosds;
import com.example.zyedu.base.BaseResult;
import com.example.zyedu.entity.Bean;
import com.example.zyedu.entity.ReportBean;
import com.example.zyedu.tools.RxSchedulers;

import rx.Observable;

/**
 * Created by acer on 2017/8/4.
 */

public class ReportModlel implements ReportContract.Modle{
    // TODO: 2017/8/11 M层提交课时上报列表数据的网络请求
    @Override
    public Observable<BaseResult> reportclass(Bean<ReportBean> beanBean) {
        return HttpMethosds.getInstance().retrofitServicel
                .reportclass(beanBean)
                .compose(RxSchedulers.io_main());
    }
}
