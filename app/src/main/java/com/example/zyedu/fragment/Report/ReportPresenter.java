package com.example.zyedu.fragment.Report;

import android.util.Log;

import com.example.zyedu.entity.Bean;
import com.example.zyedu.entity.ReportBean;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by acer on 2017/8/4.
 */

public class ReportPresenter extends ReportContract.Presenter{
    // TODO: 2017/8/11 P层对课时做M和V层的协调
    @Override
    protected void reportclass(Bean<ReportBean> beanBean) {
        rxManager.add(modle.reportclass(beanBean)
                .subscribe(result -> {
                           view.reportSuccess();
                            try {
                                Log.i("1", result.toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        e -> {
                          view.reportFail();
                            Log.i("1", e.toString());
                        }));
    }

    public String getTime(){
        return  spTools.getString("reporttime");
    }
}
