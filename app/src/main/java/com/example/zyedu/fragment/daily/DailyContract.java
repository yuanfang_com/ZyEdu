package com.example.zyedu.fragment.daily;

import com.example.zyedu.base.BaseModel;
import com.example.zyedu.base.BasePresenter;
import com.example.zyedu.base.Baseview;
import com.example.zyedu.entity.Bean;
import com.example.zyedu.entity.DailyBean;
import com.example.zyedu.entity.Daily_result;

import rx.Observable;

/**
 * Created by acer on 2017/8/4.
 */

public interface DailyContract {
    interface Modle extends BaseModel {
        Observable<Daily_result> doUpto(Bean<DailyBean> beanBean);

    }

    interface View extends Baseview {
        void reportsuccess(String a, int b);

        void reportfail(String a);

        void itmeSelect(String i, String n);

        void initDate(String name, String id, String stage);
    }

    abstract class Presenter extends BasePresenter<Modle, View> {
        public abstract void doUpto(Bean<DailyBean> beanBean);

        public abstract void saveNameStage(String name, String id, String stage,String time);

        public abstract void onCreat();
    }
}
