package com.example.zyedu.fragment.daily;

import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zyedu.R;
import com.example.zyedu.base.BaseFragment;
import com.example.zyedu.entity.Bean;
import com.example.zyedu.entity.DailyBean;
import com.example.zyedu.view.CustomDatePicker;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.example.zyedu.R.id.daily_upto_bt;

/**
 * Created by acer on 2017/8/4.
 */

public class DailyFragment extends BaseFragment<DailyPresenter, DailyModlel> implements DailyContract.View, PopupMenu.OnMenuItemClickListener {
    private String course_id;
    private String course_stage;
    private String course_name;
    private TextView daily_title_remark;
    private TextView daily_data;
    private TextView daily_class_name;
    private TextView daily_class_stage;
    private EditText dai_num_editText;
    private EditText report_content_editText2;
    private EditText student_situation;
    private EditText dai_remark_et;
    private Button daily_upto_button;
    private CustomDatePicker customDatePicker;
    private ImageView daily_data_bt;
    private ImageView daily_class_name_bt;
    private ImageView daily_class_stage_bt;
    private PopupMenu pop;
    private Bean<DailyBean> dailyBeanBean;


    @Override
    public int getLayoutId() {
        return R.layout.fragment_in_daily;
    }

    @Override
    public void initView() {
        dai_num_editText = (EditText) getActivity().findViewById(R.id.dai_num_editText);
        report_content_editText2 = (EditText) getActivity().findViewById(R.id.report_content_editText2);
        student_situation = (EditText) getActivity().findViewById(R.id.dai_student_situation_et);
        dai_remark_et = (EditText) getActivity().findViewById(R.id.dai_remark_et);
        daily_upto_button = (Button) getActivity().findViewById(daily_upto_bt);
        daily_data = (TextView) getActivity().findViewById(R.id.daily_data);
        daily_title_remark= (TextView) getActivity().findViewById(R.id.dai_title_remark);
        daily_class_name = (TextView) getActivity().findViewById(R.id.daily_class_name);
        daily_class_stage = (TextView) getActivity().findViewById(R.id.daily_class_stage);
        daily_data_bt = (ImageView) getActivity().findViewById(R.id.daily_data_bt);
        daily_class_name_bt = (ImageView) getActivity().findViewById(R.id.daily_class_name_bt);
        daily_class_stage_bt = (ImageView) getActivity().findViewById(R.id.daily_class_stage_bt);
        daily_data_bt.setOnClickListener(this);
        daily_class_name_bt.setOnClickListener(this);
        daily_class_stage_bt.setOnClickListener(this);
        daily_upto_button.setOnClickListener(this);

    }

    @Override
    public void initDate(String name, String id, String stage) {
        daily_class_name.setText(name);
        daily_class_stage.setText(stage);
        course_name = name;
        course_stage = stage;
        course_id = id;
    }

    @Override
    protected void init() {
        initDatePicker();
        dailyBeanBean = new Bean<>(context);
        presenter.onCreat();
    }

    // TODO: 2017/8/11 初始化时间选择期
    private void initDatePicker() {

        //todo 获取系统时间
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.CHINA);
        String now = sdf.format(new Date());
        daily_data.setText(now.split(" ")[0]);
        daily_title_remark.setText("");
        if(presenter.getTime().equals(now.split(" ")[0])){daily_title_remark.setText("（已填报该天日报，再次提交进行修改）");}

        customDatePicker = new CustomDatePicker(context, new CustomDatePicker.ResultHandler() {
            @Override
            public void handle(String time) {
                // todo 回调接口，获得选中的时间
                daily_data.setText(time.split(" ")[0]);
                daily_title_remark.setText("");
                if(presenter.getTime().equals(time.split(" ")[0]))daily_title_remark.setText("（已填报该天日报，再次提交进行修改）");
            }
        }, now, "2050-01-01 00:00");
        // todo 初始化日期格式请用：yyyy-MM-dd HH:mm，否则不能正常运行
        customDatePicker.showSpecificTime(false);
        // todo 显示时和分
        customDatePicker.setIsLoop(false);
        // todo 允许循环滚动
    }

    @Override
    public void reportsuccess(String a, int b) {
        new AlertDialog
                .Builder(context)
                .setTitle("提示")
                .setMessage(a)
                .setNeutralButton("确定", null)
                .show();
        if (b == 0) {
            daily_data.setTextColor(R.color.gray_8f);
            daily_class_name.setTextColor(R.color.gray_8f);
            daily_class_stage.setTextColor(R.color.gray_8f);
            dai_num_editText.setTextColor(R.color.gray_8f);
            report_content_editText2.setTextColor(R.color.gray_8f);
            student_situation.setTextColor(R.color.gray_8f);
            dai_remark_et.setTextColor(R.color.gray_8f);

        }

    }

    @Override
    public void reportfail(String a) {
        ts.show_l("上报失败");
    }

    @Override
    public void itmeSelect(String i, String n) {
        course_id = i;

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case daily_upto_bt:
                if (!sentenced_to_empty()) {
                    return;
                }
                //// TODO: 2017/8/11  执行数据提交网络请求 
                presenter.doUpto(dailyBeanBean.addDate(new DailyBean(
                        daily_data.getText().toString().trim(),
                        course_id,
                        daily_class_stage.getText().toString().trim(),
                        dai_num_editText.getText().toString().trim(),
                        report_content_editText2.getText().toString().trim(),
                        student_situation.getText().toString().trim(),
                        dai_remark_et.getText().toString().trim()
                )));
                presenter.saveNameStage(course_name, course_id, course_stage,daily_data.getText().toString().trim());
                break;
            case R.id.daily_data_bt:
                customDatePicker.show(daily_data.getText().toString());
                break;
            case R.id.daily_class_name_bt:
                // todo popmenu初始化
                pop = new PopupMenu(context, v);
                //todo 绑定控件
                pop.getMenuInflater().inflate(R.menu.daily_class_name_meun, pop.getMenu());
                //todo 设置监听
                pop.setOnMenuItemClickListener(this);
                pop.show();
                break;
            case R.id.daily_class_stage_bt:
                pop = new PopupMenu(context, v);
                pop.setOnMenuItemClickListener(this);
                pop.getMenuInflater().inflate(R.menu.daily_class_stage_menu, pop.getMenu());
                pop.show();
        }
    }

    //// TODO: 2017/8/11  判空
    private boolean sentenced_to_empty() {
        if (course_id.length() != 1) {
            return false;
        }
        if (daily_class_stage.getText().toString().equals("请点击选择")) {
            return false;
        }
        if (dai_num_editText.getText().toString().trim().length() == 0) {
            return false;
        }
        if (report_content_editText2.getText().toString().trim().length() == 0) {
            return false;
        }
        if (student_situation.getText().toString().trim().length() == 0) {
            return false;
        }
        if (dai_remark_et.getText().toString().trim().length() == 0) {
            return false;
        }
        return true;

    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (item.getGroupId() == R.id.name_group) {
            daily_class_name.setText(item.getTitle().toString());
            course_name = item.getTitle().toString();
            course_id = String.valueOf(item.getOrder()+1);
            daily_class_name.setTextColor(R.color.black);
        } else {
            daily_class_stage.setText(item.getTitle().toString());
            course_stage = item.getTitle().toString();
            daily_class_stage.setTextColor(R.color.black);
        }

        return false;
    }


}
