package com.example.zyedu.fragment.daily;

import com.example.zyedu.api.HttpMethosds;
import com.example.zyedu.entity.Bean;
import com.example.zyedu.entity.DailyBean;
import com.example.zyedu.entity.Daily_result;
import com.example.zyedu.tools.RxSchedulers;

import rx.Observable;

/**
 * Created by acer on 2017/8/4.
 */

public class DailyModlel implements DailyContract.Modle{

    @Override
    public Observable<Daily_result> doUpto(Bean<DailyBean> beanBean) {
        return HttpMethosds.getInstance().retrofitServicel
                .reportDaily(beanBean)
                .compose(RxSchedulers.io_main());
    }
}
