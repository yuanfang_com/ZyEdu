package com.example.zyedu.fragment.daily;

import android.util.Log;

import com.example.zyedu.entity.Bean;
import com.example.zyedu.entity.DailyBean;
import com.google.gson.Gson;

/**
 * Created by acer on 2017/8/4.
 */

public class DailyPresenter extends DailyContract.Presenter {


    @Override
    public void doUpto(Bean<DailyBean> beanBean) {
        Log.i("!!!!!!!!1", new Gson().toJson(beanBean));
        rxManager.add(modle.doUpto(beanBean)
                .subscribe(
                        baseResult -> {
                            view.reportsuccess(baseResult.getDes(),Integer.valueOf(baseResult.getStatus()));
                        }, e -> {

                        })
        );
    }

    @Override
    public void saveNameStage(String name, String id, String stage,String time) {
        spTools.setString("coursename", name);
        spTools.setString("courseid", id);
        spTools.setString("coursestage", stage);
        spTools.setString("coursetime",time);
    }

    @Override
    public void onCreat() {
        Log.i("11111111111",spTools.getString("coursename"));
        String a = spTools.getString("coursename");
        view.initDate(spTools.getString("coursename")
                , spTools.getString("courseid")
                , spTools.getString("coursestage")
        );
    }
   public String getTime(){
       return  spTools.getString("coursetime");
   }

}
