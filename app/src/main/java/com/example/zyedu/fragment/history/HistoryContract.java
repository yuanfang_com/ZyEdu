package com.example.zyedu.fragment.history;

import com.example.zyedu.base.BaseModel;
import com.example.zyedu.base.BasePresenter;
import com.example.zyedu.base.Baseview;

/**
 * Created by acer on 2017/8/4.
 */

public interface HistoryContract {
    interface Modle extends BaseModel{}
    interface View extends Baseview{}
    abstract class Presenter extends BasePresenter<Modle,View>{

    }
}
