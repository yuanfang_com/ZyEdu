package com.example.zyedu.fragment.history;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.view.View;
import android.widget.SimpleAdapter;

import com.example.zyedu.R;
import com.example.zyedu.adapter.MainPageview;
import com.example.zyedu.base.BaseFragment;
import com.example.zyedu.fragment.history.his_dai.History_dai_Fragment;
import com.example.zyedu.fragment.history.his_rep.History_rep_Fragment;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by acer on 2017/8/4.
 */

public class HistoryFragment extends BaseFragment<HistoryPresenter, HistoryModlel> implements HistoryContract.View {
    private History_dai_Fragment history_dai_fragment = new History_dai_Fragment();
    private History_rep_Fragment history_rep_fragment = new History_rep_Fragment();
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private MainPageview mainPageview;
    private LinkedList<Map<String, Object>> products = new LinkedList<Map<String, Object>>();
    private DefaultItemAnimator animator;
    private SimpleAdapter adapter;


    @Override
    protected void init() {
        ArrayList<Fragment> fragments = new ArrayList<Fragment>();
        fragments.add(history_dai_fragment);
        fragments.add(history_rep_fragment);
        ArrayList<String> tab_text = new ArrayList<String>();
        tab_text.add("日报记录");
        tab_text.add("课时记录");
        mainPageview = new MainPageview(getChildFragmentManager(), fragments);
        mainPageview.setTitles(tab_text);
        viewPager.setAdapter(mainPageview);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_in_history;
    }

    @Override
    public void initView() {

        tabLayout = (TabLayout) getActivity().findViewById(R.id.his_tablayout);
        viewPager = (ViewPager) getActivity().findViewById(R.id.his_viewpager);


    }


    @Override
    public void onClick(View v) {

    }


}
