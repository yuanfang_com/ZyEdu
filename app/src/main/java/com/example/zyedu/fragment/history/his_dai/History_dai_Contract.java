package com.example.zyedu.fragment.history.his_dai;

import com.example.zyedu.base.BaseModel;
import com.example.zyedu.base.BasePresenter;
import com.example.zyedu.base.Baseview;
import com.example.zyedu.entity.Bean;
import com.example.zyedu.entity.His_dai_bean;
import com.example.zyedu.entity.His_dai_result;
import com.example.zyedu.entity.His_dai_result_a;

import java.util.List;

import rx.Observable;

/**
 * Created by acer on 2017/8/4.
 */

public interface History_dai_Contract {
    interface Modle extends BaseModel{
         Observable<His_dai_result>do_his_dai(Bean<His_dai_bean> bean);
    }
    interface View extends Baseview{
    void initData(List<His_dai_result_a> data);
    }
    abstract class Presenter extends BasePresenter<Modle,View>{
      public   abstract void  do_his_dai_api(Bean<His_dai_bean> bean) ;
    }
}
