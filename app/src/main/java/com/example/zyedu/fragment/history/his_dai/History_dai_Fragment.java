package com.example.zyedu.fragment.history.his_dai;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import com.example.zyedu.R;
import com.example.zyedu.adapter.SimpleAdapter;
import com.example.zyedu.base.BaseFragment;
import com.example.zyedu.entity.Bean;
import com.example.zyedu.entity.His_dai_bean;
import com.example.zyedu.entity.His_dai_result_a;
import com.example.zyedu.view.HisDatePicker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;

/**
 * Created by acer on 2017/8/4.
 */

public class History_dai_Fragment extends BaseFragment<History_dai_Presenter, History_dai_Modlel> implements History_dai_Contract.View {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private RecyclerView recyclerView;
    private LinearLayout seacher_lin;
    private List<His_dai_result_a> apiData=new ArrayList<>();
    private DefaultItemAnimator animator;
    private SimpleAdapter adapter;
    HisDatePicker hisDatePicker;
    TextView his_dai_year;
    TextView his_dai_month;
    TextView his_dai_day;
    MultiAutoCompleteTextView his_dai_selname;


    @Override//// TODO: 2017/8/18 网络请求数据
    protected void init() {
        His_dai_bean his_dai_result = new His_dai_bean("", "", "", "");
        Bean<His_dai_bean> bean = new Bean<>(context);
        bean.addDate(his_dai_result);
        presenter.do_his_dai_api(bean);
        initDatePicker();
    }

    @Override
    public int getLayoutId() {
        return R.layout.his_dai_fragment;
    }

    @SuppressLint("WrongViewCast")
    @Override
    public void initView() {
        seacher_lin= (LinearLayout) getActivity().findViewById(R.id.seacher);
        his_dai_year = (TextView) getActivity().findViewById(R.id.his_dai_year);
        his_dai_month = (TextView) getActivity().findViewById(R.id.his_dai_month);
        his_dai_day = (TextView) getActivity().findViewById(R.id.his_dai_day);
        his_dai_selname = (MultiAutoCompleteTextView)getActivity().findViewById(R.id.his_rep_selname);
        his_dai_year.setOnClickListener(this);
        his_dai_month.setOnClickListener(this);
        his_dai_day.setOnClickListener(this);
        seacher_lin.setOnClickListener(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initWidget(view);

    }
   //// TODO: 2017/8/18  初始化list
    private void initWidget(View view) {
        recyclerView = (RecyclerView) getActivity().findViewById(R.id.recyclerview);
        //setHasFixedSize 的作用就是确保尺寸是通过用户输入从而确保RecyclerView的尺寸是一个常数。
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(animator);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new SimpleAdapter(getActivity(),apiData );
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        // TODO: 2017/8/22 点击选择查询日期
        if (his_dai_year.getText().toString().equals("")) {
            hisDatePicker.show(new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.CHINA)
                    .format(new Date())
                    .split(" ")[0]);
        } else if (his_dai_month.getText().toString().equals("")) {
            hisDatePicker.show(his_dai_year.getText().toString() + "-01-01");
        } else if(his_dai_day.getText().toString().equals("")){
            hisDatePicker.show(his_dai_year.getText()
                    .toString() + "-" + his_dai_month
                    .getText()
                    .toString() + "-01");}
        else{
            hisDatePicker.show(his_dai_year
                    .getText()
                    .toString() + "-" + his_dai_month
                    .getText()
                    .toString() + "-"+his_dai_day
                    .getText()
                    .toString());
        }



    }



    @Override
    public void initData(List<His_dai_result_a> data) {
        if(data!=null&&data.size()>=0){
            Log.i("1","sssssssss");
            apiData.clear();
            apiData.addAll(data);
            adapter.notifyDataSetChanged();
        }

    }
    private void initDatePicker() {

        hisDatePicker = new HisDatePicker(context, new HisDatePicker.ResultHandler() {
            @Override
            public void handle(String year, String month, String day) {
                his_dai_year.setText(year);
                his_dai_month.setText(month);
                his_dai_day.setText(day);
            }
        }, "2017-01-01 00:00", "2050-01-01 00:00");
        // todo 初始化日期格式请用：yyyy-MM-dd HH:mm，否则不能正常运行
        hisDatePicker.showSpecificTime(false);
        // todo 显示时和分
        hisDatePicker.setIsLoop(false);
        // todo 允许循环滚动
    }


}
