package com.example.zyedu.fragment.history.his_dai;

import com.example.zyedu.api.HttpMethosds;
import com.example.zyedu.entity.Bean;
import com.example.zyedu.entity.His_dai_bean;
import com.example.zyedu.entity.His_dai_result;
import com.example.zyedu.tools.RxSchedulers;

import rx.Observable;

/**
 * Created by acer on 2017/8/4.
 */

public class History_dai_Modlel implements History_dai_Contract.Modle{

    @Override
    public Observable<His_dai_result> do_his_dai(Bean<His_dai_bean> bean) {
        return HttpMethosds.getInstance().retrofitServicel
                .hisDaily(bean)
                .compose(RxSchedulers.io_main());
    }
}
