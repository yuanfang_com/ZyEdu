package com.example.zyedu.fragment.history.his_rep;

import com.example.zyedu.base.BaseModel;
import com.example.zyedu.base.BasePresenter;
import com.example.zyedu.base.BaseResult;
import com.example.zyedu.base.Baseview;
import com.example.zyedu.entity.Bean;
import com.example.zyedu.entity.Requstreport;
import com.example.zyedu.entity.TeacherReportDayVO;

import java.util.List;
import java.util.Map;

import rx.Observable;

/**
 * Created by acer on 2017/8/4.
 */

public interface History_rep_Contract {
    interface Modle extends BaseModel {
        // TODO: 2017/8/22 网络请求汇总数据
        Observable<BaseResult<List<TeacherReportDayVO>>> requstrep(Bean<Requstreport> beanBean);

        // TODO: 2017/8/22 网络请求某天数据
        Observable<BaseResult<List<TeacherReportDayVO>>> requstrepday(Bean<Requstreport> beanBean);
    }

    interface View extends Baseview {
        // TODO: 2017/8/22 绑定汇总数据
        void binrecy(List<TeacherReportDayVO> data);

        // TODO: 2017/8/22 绑定某天数据
        void binrecyday(List<TeacherReportDayVO> data);
    }

    abstract class Presenter extends BasePresenter<Modle, View> {
        // TODO: 2017/8/22 课时汇总请求
        protected abstract void requstrep(Bean<Requstreport> beanBean);

        // TODO: 2017/8/22 课时某天请求
        protected abstract void requstrepday(Bean<Requstreport> beanBean);
    }
}
