package com.example.zyedu.fragment.history.his_rep;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import com.example.zyedu.R;
import com.example.zyedu.adapter.Recycleradapter;
import com.example.zyedu.base.BaseFragment;
import com.example.zyedu.entity.Bean;
import com.example.zyedu.entity.Requstreport;
import com.example.zyedu.entity.TeacherReportDayVO;
import com.example.zyedu.view.CustomDatePicker;
import com.example.zyedu.view.HisDatePicker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by acer on 2017/8/4.
 */

public class History_rep_Fragment extends BaseFragment<History_rep_Presenter, History_rep_Modlel> implements History_rep_Contract.View {
    RecyclerView recyclerView;
    Recycleradapter recycleradapter;
    List<TeacherReportDayVO> list = new ArrayList<TeacherReportDayVO>();
    HisDatePicker hisDatePicker;
    TextView his_sel_year;
    TextView his_sel_month;
    TextView his_sel_day;
    MultiAutoCompleteTextView his_rep_selname;
    Button his_rep_select;

    @Override
    protected void init() {
        // TODO: 2017/8/22 查询所有汇总
        presenter.requstrep(new Bean<Requstreport>(context).addDate(new Requstreport("", "", "", "")));
        initDatePicker();
    }

    @Override
    public int getLayoutId() {
        return R.layout.his_rep_fragment;
    }

    @SuppressLint("WrongViewCast")
    @Override
    public void initView() {
        recyclerView = (RecyclerView) getActivity().findViewById(R.id.his_rep_rec);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recycleradapter = new Recycleradapter(context, list, recyclerView, presenter, 1);
        recyclerView.setAdapter(recycleradapter);
        his_sel_year = (TextView) getActivity().findViewById(R.id.his_sel_year);
        his_sel_month = (TextView) getActivity().findViewById(R.id.his_sel_month);
        his_sel_day = (TextView) getActivity().findViewById(R.id.his_sel_day);
        his_rep_selname = (MultiAutoCompleteTextView) getActivity().findViewById(R.id.his_rep_selname);
        his_rep_select= (Button) getActivity().findViewById(R.id.his_rep_select);
        his_sel_year.setOnClickListener(this);
        his_sel_month.setOnClickListener(this);
        his_sel_day.setOnClickListener(this);
        his_rep_select.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.his_rep_select:
                // TODO: 2017/8/22 单击查询按钮查询天日期文本为空查询课时汇总数据，否则查询某天数据
                if (his_sel_day.getText().toString().equals(""))
                    presenter.requstrep(
                            new Bean<Requstreport>(context)
                                    .addDate(new Requstreport(his_rep_selname
                                            .getText().toString()
                                            , his_sel_year
                                            .getText()
                                            .toString()
                                            , his_sel_month
                                            .getText().toString()
                                            , his_sel_day.getText()
                                            .toString())));
                else
                    presenter.requstrepday(new Bean<Requstreport>(context)
                            .addDate(new Requstreport(his_rep_selname
                                    .getText().toString()
                                    , his_sel_year.getText()
                                    .toString()
                                    , his_sel_month
                                    .getText().toString()
                                    , his_sel_day
                                    .getText()
                                    .toString())));
                break;
            default:
                // TODO: 2017/8/22 点击选择查询日期
                if (his_sel_year.getText().toString().equals("")) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
                    String now = sdf.format(new Date());
                    hisDatePicker.show(now);
                } else if (his_sel_month.getText().toString().equals("")) {
                    hisDatePicker.show(his_sel_year.getText().toString() + "-01-01");
                } else if(his_sel_day.getText().toString().equals("")){
                    hisDatePicker.show(his_sel_year.getText()
                            .toString() + "-" + his_sel_month
                            .getText()
                            .toString() + "-01");}
                else{
                    hisDatePicker.show(his_sel_year
                            .getText()
                            .toString() + "-" + his_sel_month
                            .getText()
                            .toString() + "-"+his_sel_day
                            .getText()
                            .toString());
                }
        }
    }

    // TODO: 2017/8/22 动态绑定汇总数据
    @Override
    public void binrecy(List<TeacherReportDayVO> data) {
        if (data != null && data.size() >= 0) {
            list.clear();
            list.addAll(data);
            recycleradapter.state = 1;
            recycleradapter.notifyDataSetChanged();
        }
    }
    // TODO: 2017/8/22 动态绑定某天数据
    @Override
    public void binrecyday(List<TeacherReportDayVO> data) {
        if (data != null && data.size() >= 0) {
            list.clear();
            list.addAll(data);
            recycleradapter.state = 2;
            recycleradapter.notifyDataSetChanged();
        }
    }
    // TODO: 2017/8/22 初始化 日期选择器
    private void initDatePicker() {

        hisDatePicker = new HisDatePicker(context, new HisDatePicker.ResultHandler() {
            @Override
            public void handle(String year, String month, String day) {
                his_sel_year.setText(year);
                his_sel_month.setText(month);
                his_sel_day.setText(day);
            }
        },
                "2017-01-01 00:00", "2050-01-01 00:00");

        // todo 初始化日期格式请用：yyyy-MM-dd HH:mm，否则不能正常运行
        hisDatePicker.showSpecificTime(false);
        // todo 显示时和分
        hisDatePicker.setIsLoop(false);
        // todo 允许循环滚动
    }

    // TODO: 2017/8/22 选择器选择日期回调事件
    public void setdate(String year, String month, String day) {
        his_sel_year.setText(year);
        his_sel_month.setText(month);
        his_sel_day.setText(day);
    }
}
