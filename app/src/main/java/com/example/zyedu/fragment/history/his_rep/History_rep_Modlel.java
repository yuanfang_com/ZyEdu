package com.example.zyedu.fragment.history.his_rep;

import com.example.zyedu.api.HttpMethosds;
import com.example.zyedu.base.BaseResult;
import com.example.zyedu.entity.Bean;
import com.example.zyedu.entity.Requstreport;
import com.example.zyedu.entity.TeacherReportDayVO;
import com.example.zyedu.tools.RxSchedulers;

import java.util.List;
import java.util.Map;

import rx.Observable;

/**
 * Created by acer on 2017/8/4.
 */

public class History_rep_Modlel implements History_rep_Contract.Modle{
    // TODO: 2017/8/22   M层查询课时汇总数据的网络请求
    @Override
    public Observable<BaseResult<List<TeacherReportDayVO>>> requstrep(Bean<Requstreport> beanBean) {
        return HttpMethosds.getInstance().retrofitServicel
                .requstrep(beanBean)
                .compose(RxSchedulers.io_main());
    }
    // TODO: 2017/8/22   M层查询课时某日数据的网络请求
    @Override
    public Observable<BaseResult<List<TeacherReportDayVO>>> requstrepday(Bean<Requstreport> beanBean) {
        return HttpMethosds.getInstance().retrofitServicel
                .requstrepday(beanBean)
                .compose(RxSchedulers.io_main());
    }
}
