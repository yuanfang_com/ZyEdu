package com.example.zyedu.fragment.history.his_rep;

import android.util.Log;

import com.example.zyedu.entity.Bean;
import com.example.zyedu.entity.Requstreport;
import com.google.gson.Gson;

/**
 * Created by acer on 2017/8/4.
 */

public class History_rep_Presenter extends History_rep_Contract.Presenter{
    // TODO: 2017/8/22 P层对课时汇总数据做M和V层的协调
    @Override
    protected void requstrep(Bean<Requstreport> beanBean) {
        rxManager.add(modle.requstrep(beanBean)
                .subscribe(result -> {
                            try {
                                Log.i("1",""+new Gson().toJson(result.getData()));
                                view.binrecy(result.getData());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        e -> {
                            Log.i("1", e.toString());
                        }));
    }
    // TODO: 2017/8/22 P层对课时某天数据做M和V层的协调
    @Override
    public void requstrepday(Bean<Requstreport> beanBean) {
        rxManager.add(modle.requstrepday(beanBean)
                .subscribe(result -> {
                            try {
                                Log.i("1",""+new Gson().toJson(result.getData()));
                                view.binrecyday(result.getData());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        e -> {
                            Log.i("1", e.toString());
                        }));
    }
}
