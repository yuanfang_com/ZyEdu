package com.example.zyedu.tools;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.zyedu.adapter.Recycleradapter;
import com.example.zyedu.anim.HisrepViewHolderAnimator;


/**
 * Created by JunX on 17/5/13.
 */

public class ExpandableViewHoldersHisrep {

    public static void open(final Recycleradapter.HisrepViewHolder holder, final View expandView, final boolean animate) {
        if (animate) {
            expandView.setVisibility(View.VISIBLE);
            final Animator animator = HisrepViewHolderAnimator.ofItemViewHeight(holder);
            animator.start();
        }
        else {
            expandView.setVisibility(View.VISIBLE);
        }
    }

    public static void close(final Recycleradapter.HisrepViewHolder holder, final View expandView, final boolean animate) {
        if (animate) {
            expandView.setVisibility(View.GONE);
            final Animator animator = HisrepViewHolderAnimator.ofItemViewHeight(holder);
            expandView.setVisibility(View.VISIBLE);
            animator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    expandView.setVisibility(View.GONE);
                }
            });
            animator.start();
        }
        else {
            expandView.setVisibility(View.GONE);
        }
    }

    public static void open(final Recycleradapter.HisrepViewHolder holder, final View expandView, final View expandTopView, final boolean animate) {
        if (animate) {
            expandView.setVisibility(View.VISIBLE);
            expandTopView.setVisibility(View.VISIBLE);
            final Animator animator = HisrepViewHolderAnimator.ofItemViewHeight(holder);
            animator.start();
            final Animator animator2 = HisrepViewHolderAnimator.ofItemViewHeight2(holder, true);
            animator2.start();
        }
        else {
            expandView.setVisibility(View.VISIBLE);
            expandTopView.setVisibility(View.VISIBLE);
        }
    }

    public static void close(final Recycleradapter.HisrepViewHolder holder, final View expandView, final View expandTopView, final boolean animate) {
        if (animate) {
            expandView.setVisibility(View.GONE);
            expandTopView.setVisibility(View.VISIBLE);

            final Animator animator = HisrepViewHolderAnimator.ofItemViewHeight(holder);
            expandView.setVisibility(View.VISIBLE);

            final Animator animator2 = HisrepViewHolderAnimator.ofItemViewHeight2(holder, false);
            expandTopView.setVisibility(View.VISIBLE);

            animator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    expandView.setVisibility(View.GONE);
                }
            });

            animator2.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    expandTopView.setVisibility(View.VISIBLE);
                }
            });

            animator.start();
            animator2.start();
        }
        else {
            expandView.setVisibility(View.GONE);
            expandTopView.setVisibility(View.VISIBLE);
        }
    }



    public interface Expandable {
        View getExpandView();
    }

    public interface ExpandableTop {
        View getExpandTopView();
    }

    public static class OpenItem<ViewHolder extends Recycleradapter.HisrepViewHolder & Expandable & ExpandableTop> {
        private int _opened = -1;

        public void bind(ViewHolder holder, int pos) {
            if (pos == _opened) {
                if (pos != 0)
                    ExpandableViewHoldersHisrep.open(holder, holder.getExpandView(), holder.getExpandTopView(), false);
                else
                    ExpandableViewHoldersHisrep.open(holder, holder.getExpandView(), false);
            }
            else {
                if (pos != 0)
                    ExpandableViewHoldersHisrep.close(holder, holder.getExpandView(), holder.getExpandTopView(), false);
                else
                    ExpandableViewHoldersHisrep.close(holder, holder.getExpandView(), false);
            }
        }

        public void toggle(ViewHolder holder) {
            if (_opened == holder.getPosition()) {
                _opened = -1;
                ExpandableViewHoldersHisrep.close(holder, holder.getExpandView(), holder.getExpandTopView(), true);
            }
            else {
                int previous = _opened;
                _opened = holder.getPosition();
                if (holder.getPosition() != 0)
                    ExpandableViewHoldersHisrep.open(holder, holder.getExpandView(), holder.getExpandTopView(),true);
                else
                    ExpandableViewHoldersHisrep.open(holder, holder.getExpandView(),true);

                final ViewHolder oldHolder = (ViewHolder) ((RecyclerView) holder.itemView.getParent()).findViewHolderForPosition(previous);
                if (oldHolder != null) {
                    if (previous != 0)
                        ExpandableViewHoldersHisrep.close(oldHolder, oldHolder.getExpandView(), oldHolder.getExpandTopView(), true);
                    else
                        ExpandableViewHoldersHisrep.close(oldHolder, oldHolder.getExpandView(),true);
                }
            }
        }
    }


}
