package com.example.zyedu.tools;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.zyedu.activity.login.LoginActivity;
import com.example.zyedu.activity.main.MainActivity;
import com.example.zyedu.activity.register.RegisterActvity;

/**
 * Created by 半生瓜 on 2017/7/20.
 */

public class JumpUtil {

    public static void toLogin(Context context){
        Intent intent=new Intent(context,LoginActivity.class);
        context.startActivity(intent);
    }
    public static void toMain(Context context){
        Intent intent=new Intent(context,MainActivity.class);
        context.startActivity(intent);
    }
    public  static void to(Context context,Class  c){
        Intent intent=new Intent(context,c);
        context.startActivity(intent);
    }
    public  static void toLogin1(Context context){
        Intent intent=new Intent(context,LoginActivity.class);
        intent.putExtra("requsecode",1);
        context.startActivity(intent);
    }
    public static void toRegister(Context context){
        Intent intent=new Intent(context,RegisterActvity.class);
        context.startActivity(intent);
    }
}
