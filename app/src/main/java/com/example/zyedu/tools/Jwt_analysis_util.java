package com.example.zyedu.tools;


import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.example.zyedu.entity.Login_result_jwt;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;


/**
 * Created by 半生瓜 on 2017/8/16.
 */

public class Jwt_analysis_util {

   // @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public  static  String analysis_name (String jwt)  {


        String strBase64 = null;
        try {
            strBase64 = new String(Base64.getUrlDecoder().decode(jwt.split("\\.")[1]), "UTF8").toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Log.i("333333333333333333",strBase64);
        Gson gson = new Gson();
        Login_result_jwt login_result_jwt= gson.fromJson(strBase64,Login_result_jwt.class);

        return login_result_jwt.getName();
    }


}
