package com.example.zyedu.tools;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by 半生瓜 on 2017/8/11.
 */

public class SpTools {

    static SharedPreferences sp;
    public SpTools(Context context){
        sp = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setString(String key,String value){
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(key,value);
        editor.apply();
    }
    public void setBoolean(String key,Boolean value){
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(key,value);
        editor.apply();
    }
   public String getString(String key){
       return sp.getString(key,"");
   }
    public Boolean getBoolean(String key){
        return sp.getBoolean(key,false);
    }

}
