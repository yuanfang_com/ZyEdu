package com.example.zyedu.view;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.zyedu.R;


public class LoadingDialog extends Dialog {
	private TextView mTip;
	private ProgressBar mBar;

	public LoadingDialog(Context context) {
		super(context);
		init();
	}

	public LoadingDialog(Context context, int theme) {
		super(context, theme);
		init();
	}
	public void setTipText(String tip) {
		mTip.setText(tip);
	}
	private void init() {


	}

}
